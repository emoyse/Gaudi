stages:
  - build
  - test
  - deploy

image: gitlab-registry.cern.ch/lhcb-core/lbdocker/centos7-build

variables:
  NO_LBLOGIN: "1"
  TARGET_BRANCH: master
  BINARY_TAG: x86_64-centos7-gcc7-opt
  BUILDDIR: build-opt
  TESTS_REPORT: "test_report"
  LCG_hostos: "x86_64-centos7"
  LCG_release_area: "/cvmfs/sft.cern.ch/lcg/releases"
  LCG_contrib: "/cvmfs/sft.cern.ch/lcg/contrib"
  CLANG_FORMAT_VERSION: "3.9"
  CCACHE_VERSION: "3.3.4-e92e5"

build:gcc7:opt:
  stage: build
  tags:
    - cvmfs
  variables:
    BINARY_TAG: x86_64-centos7-gcc7-opt
    BUILDDIR: build-opt
  script:
    - ci-utils/build
  artifacts:
    paths:
      - ${BUILDDIR}
      - .ccache
    expire_in: 1 week

build:gcc7:dbg:
  stage: build
  tags:
    - cvmfs
  variables:
    BINARY_TAG: x86_64-centos7-gcc7-dbg
    BUILDDIR: build-dbg
  script:
    - ci-utils/build
  artifacts:
    paths:
      - ${BUILDDIR}
      - .ccache
    expire_in: 1 week

build:gcc62:opt:
  stage: build
  tags:
    - cvmfs
  variables:
    BINARY_TAG: x86_64-centos7-gcc62-opt
    BUILDDIR: build-gcc62
  script:
    - ci-utils/build
  artifacts:
    paths:
      - ${BUILDDIR}
      - .ccache
    expire_in: 1 week

check-formatting:
  stage: test
  dependencies: []
  tags:
    - cvmfs
  variables:
    BINARY_TAG: x86_64-centos7-gcc7-opt
    BUILDDIR: build-opt
  script:
    - ci-utils/check-formatting
  artifacts:
    paths:
      - apply-formatting.patch
    when: on_failure
    expire_in: 1 day

doxygen:
  stage: test
  dependencies:
    - build:gcc7:opt
  tags:
    - cvmfs
  only:
    - master
    - tags
  variables:
    BINARY_TAG: x86_64-centos7-gcc7-opt
    BUILDDIR: build-opt
  script:
    - . ci-utils/env_setup.sh
    - find ${BUILDDIR} -type f -exec touch -d $(date +@%s) \{} \;
    - make BUILDDIR=${BUILDDIR} doc
    - rm -rf public
    - mkdir -p public/doxygen
    - mv ${BUILDDIR}/doxygen/html ${CI_COMMIT_REF_SLUG}
    - zip -r -q public/doxygen/${CI_COMMIT_REF_SLUG}.zip ${CI_COMMIT_REF_SLUG}
  artifacts:
    paths:
      - public
    expire_in: 1 day

test:gcc7:opt:
  stage: test
  dependencies:
    - build:gcc7:opt
  tags:
    - cvmfs
  variables:
    BINARY_TAG: x86_64-centos7-gcc7-opt
    BUILDDIR: build-opt
  script:
    - ci-utils/test
  artifacts:
    paths:
      - ${TESTS_REPORT}
    when: always
    expire_in: 1 week

test:gcc7:dbg:
  stage: test
  dependencies:
    - build:gcc7:dbg
  tags:
    - cvmfs
  variables:
    BINARY_TAG: x86_64-centos7-gcc7-dbg
    BUILDDIR: build-dbg
  script:
    - ci-utils/test
  artifacts:
    paths:
      - ${TESTS_REPORT}
    when: always
    expire_in: 1 week

test:gcc62:opt:
  stage: test
  dependencies:
    - build:gcc62:opt
  tags:
    - cvmfs
  variables:
    BINARY_TAG: x86_64-centos7-gcc62-opt
    BUILDDIR: build-gcc62
    TESTS_REPORT: test_report-gcc62
  script:
    - ci-utils/test
  artifacts:
    paths:
      - ${TESTS_REPORT}
    when: always
    expire_in: 1 week

test_public_headers_build:
  stage: test
  dependencies:
    - build:gcc7:opt
  tags:
    - cvmfs
  variables:
    BINARY_TAG: x86_64-centos7-gcc7-opt
    BUILDDIR: build-opt
  script:
    - ci-utils/test_public_headers_build
  artifacts:
    paths:
      - .ccache
    expire_in: 1 week

# see https://gitlab.cern.ch/gitlabci-examples/deploy_eos for the details
# of the configuration
deploy-doxygen:
  stage: deploy
  dependencies:
    - doxygen
  only:
    - master
    - tags
  image: gitlab-registry.cern.ch/ci-tools/ci-web-deployer:latest
  script:
    - test -z "$EOS_ACCOUNT_USERNAME" -o -z "$EOS_ACCOUNT_PASSWORD" -o -z "$EOS_PATH" && exit 0 || true
    # Script that performs the deploy to EOS. Makes use of the variables defined in the project
    # It will copy the generated content to the folder in EOS
    - deploy-eos
  # do not run any globally defined before_script or after_script for this step
  before_script: []
  after_script: []
